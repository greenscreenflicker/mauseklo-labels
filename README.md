Maueseklo Labeler
----------------

The tool allows to label diverse sortiments of Mauseklos. These are specific containers for small smd components.

ELV SMD Boxes:
--------------
Width x Height
1x1: 12mm x 15mm
1x2: 27.5mm x 15mm
2x2: 27.5mm x 41mm
4x2: 59mm x 41mm


Requirements
-------------
Required Programs: 
	Python (tested with 3.10.7 as of 28.09.2022)
	DB Browser for SQLite
-Install Python version 3.10.7 from [https://www.python.org/downloads/] via the installer
-Install DB Browser for SQLLite from [https://sqlitebrowser.org/dl/] via the installer
-open windows command console
-check if windows recognizes python by entering "py --version" into the console, it should return the current version
-check if windows recognizes pip by entering "py -m pip --version" into the console, it should return the current pip version
-install reportlab by entering "py -m pip install reportlab" into the console, the installation process should be visible

Run
--------------
Open DB Brower and drag and drop the file "printdb" from the repository into the big white field. Switch to the tab "search data"
to edit the values in the text fields.

Open the python IDLE shell and select "Open..." under "File", and select "main.py" from the repository.
Adjust the height and width values for your smd boxes if neccessary, they can be found in line 65,66 under "_boxw" and "_boxh".
Run "main.py" and observe the output file "labels".

This can easily be printed by a normal A4 printer. Use adhesive tape to fix it to the boxes.
Just configure your custom width.

Fixing on SMD Boxes
----------------------
Gluestick does not work reliably. Really use adhesive tape to fix it to the boxes.


